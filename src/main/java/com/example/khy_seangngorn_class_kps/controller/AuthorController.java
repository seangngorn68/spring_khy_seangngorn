package com.example.khy_seangngorn_class_kps.controller;

import com.example.khy_seangngorn_class_kps.model.ApiResponse;
import com.example.khy_seangngorn_class_kps.model.Author;
import com.example.khy_seangngorn_class_kps.service.AuthorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping ("/api/v1/")
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/authors")
    public List<Author> getAllAuthor(){
        return authorService.getAllAuthor();
    }
    @GetMapping("/authors/{author_id}")
    public ResponseEntity<?> getAuhtorById(@PathVariable Integer author_id){
        ApiResponse<Author> apiResponse=new ApiResponse<>(
                        LocalDateTime.now(),
                        HttpStatus.OK,
                        "Success get data by id",
                        authorService.getAuthorById(author_id)
        );
        return ResponseEntity.ok(apiResponse);
    }
    @PostMapping("/authors")
    public ResponseEntity<?> insertAuthor(@RequestBody Author author){
        return ResponseEntity.ok(new ApiResponse<Author>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Insert Successfully author",
                authorService.insertAuthor(author)
        ));
    }
    @PutMapping("/UpudateAuthorById/{author_id}")
    public ResponseEntity<?> updateAuthor(@PathVariable Integer author_id,@RequestBody Author author){
        authorService.updateAuthor(author_id,author);
        return ResponseEntity.ok(new ApiResponse<Author>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Update Successfully",
                null
        ));
    }
    @DeleteMapping("authors/{author_id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable Integer author_id){
        authorService.deleteAuthor(author_id);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully Delete data",
                null));
    }
}
