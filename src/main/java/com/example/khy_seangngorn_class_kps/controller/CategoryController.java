package com.example.khy_seangngorn_class_kps.controller;

import com.example.khy_seangngorn_class_kps.model.ApiResponse;
import com.example.khy_seangngorn_class_kps.model.Category;
import com.example.khy_seangngorn_class_kps.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class CategoryController {
    private final CategoryService categoryService;
    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping("/categorys")
    public List<Category> getAllCategory(){
        return categoryService.getAllCategory();
    }
    @GetMapping("/categorys/{category_id}")
    public ResponseEntity<?> getCategoryById(@PathVariable  Integer category_id){
        ApiResponse<Category> apiResponse=new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Success get data by id",
                categoryService.getCategoryById(category_id)
        );
        return ResponseEntity.ok(apiResponse);
    }
    @PostMapping("/categorys")
    public ResponseEntity<?> insertCategory(@RequestBody Category category){
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Insert Successfully Data",
                categoryService.insertCategory(category)
        ));
    }
    @PutMapping("/update_Category_ById/{category_id}")
    public ResponseEntity<?> updateCategoryById(@PathVariable Integer category_id,@RequestBody Category category){
        categoryService.updateCategory(category_id,category);
        return ResponseEntity.ok(new ApiResponse<Category>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Update Successfully Data",
               null
        ));
    }
    @DeleteMapping("category/{category_id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable Integer category_id){
        categoryService.deleteCategory(category_id);
        categoryService.deleteCategory(category_id);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully Delete data",
                null));
    }
}
