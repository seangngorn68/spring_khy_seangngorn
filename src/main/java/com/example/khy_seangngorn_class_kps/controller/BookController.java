package com.example.khy_seangngorn_class_kps.controller;

import com.example.khy_seangngorn_class_kps.model.ApiResponse;
import com.example.khy_seangngorn_class_kps.model.Book;
import com.example.khy_seangngorn_class_kps.model.BookRequest;
import com.example.khy_seangngorn_class_kps.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
@RestController
@RequestMapping("/api/v1/")
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }
    @GetMapping("/books")
    public ResponseEntity<?> getAllBook(){
        return ResponseEntity.ok(new ApiResponse<List<Book>>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Success To get all data",
                bookService.getAllBook()
        ));
    }
    @GetMapping("/book/{book_id}")
    public ResponseEntity<?> getBookById(@PathVariable Integer book_id){
        ApiResponse<Book> apiResponse=new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Success To get By data",
                bookService.getBookById(book_id)
        );
        return ResponseEntity.ok(apiResponse);
    }
    @PostMapping("/books")
    public ResponseEntity<?> insertBooks(@RequestBody BookRequest bookRequest){
        bookService.insertBook(bookRequest);
        return ResponseEntity.ok(new ApiResponse<List<Book>>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Success insert data",
              null
        ));
    }
    @PutMapping("/updateById/{book_id}")
    public ResponseEntity<?> updateBooks(@PathVariable Integer book_id,@RequestBody BookRequest bookRequest){
        bookService.updateBook(book_id,bookRequest);
        return ResponseEntity.ok(new ApiResponse<List<Book>>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Success update data",
                null
        ));
    }
    @DeleteMapping("book/{book_id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable Integer book_id){
        bookService.deleteBook(book_id);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully Delete data",
                null));
    }
}
