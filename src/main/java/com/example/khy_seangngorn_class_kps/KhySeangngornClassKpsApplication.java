package com.example.khy_seangngorn_class_kps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KhySeangngornClassKpsApplication {

    public static void main(String[] args) {
        SpringApplication.run(KhySeangngornClassKpsApplication.class, args);
    }

}
