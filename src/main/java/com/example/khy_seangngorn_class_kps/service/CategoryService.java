package com.example.khy_seangngorn_class_kps.service;

import com.example.khy_seangngorn_class_kps.model.Author;
import com.example.khy_seangngorn_class_kps.model.Category;
import java.util.List;

public interface CategoryService {
    List <Category> getAllCategory();

    List<Category> getAllCategory(Integer book_id);

    Category getCategoryById(Integer category_id);
     Category insertCategory(Category category);
     void updateCategory(Integer category_id,Category category);
     void deleteCategory(Integer category_id);
}
