package com.example.khy_seangngorn_class_kps.service;

import com.example.khy_seangngorn_class_kps.model.Book;
import com.example.khy_seangngorn_class_kps.model.BookRequest;

import java.util.List;

public interface BookService {
    List<Book>getAllBook();

    Book getBookById(Integer book_id);

   void insertBook(BookRequest bookRequest);

   void updateBook(Integer book_id,BookRequest bookRequest);
    void deleteBook(Integer book_id);
}
