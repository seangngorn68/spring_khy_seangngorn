package com.example.khy_seangngorn_class_kps.service;

import com.example.khy_seangngorn_class_kps.model.Category;
import com.example.khy_seangngorn_class_kps.reposithory.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImp implements CategoryService {
    private CategoryRepo categoryRepo;
    @Autowired
    public CategoryServiceImp(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }



    @Override
    public List<Category> getAllCategory() {
        return categoryRepo.getAllCategory();
    }

//    @Override
//    public List<Category> getAllCategory() {
//        return categoryRepo.get;
//    }

    @Override
    public List<Category> getAllCategory(Integer book_id) {
        return categoryRepo.getAllBook(book_id);
    }


    @Override
    public Category getCategoryById(Integer category_id) {
        return categoryRepo.getCategoryById(category_id);
    }

    @Override
    public Category insertCategory(Category category) {
        return categoryRepo.insertCategory(category);
    }

    @Override
    public void updateCategory(Integer category_id, Category category) {
        categoryRepo.updateCategory(category_id,category);
    }

    @Override
    public void deleteCategory(Integer category_id) {
        categoryRepo.deleteCategory(category_id);
    }
}
