package com.example.khy_seangngorn_class_kps.service;

import com.example.khy_seangngorn_class_kps.exception.BlankFieldExceptionHandler;
import com.example.khy_seangngorn_class_kps.model.Author;
import com.example.khy_seangngorn_class_kps.reposithory.AuthorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AuthorServiceImp implements AuthorService {
    private AuthorRepo authorRepo;
    @Autowired
    public AuthorServiceImp(AuthorRepo authorRepo) {
        this.authorRepo = authorRepo;
    }

    @Override
    public List<Author> getAllAuthor() {
        return authorRepo.getAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer author_id) {
        return authorRepo.getAuthorById(author_id);
    }

    @Override
    public Author insertAuthor(Author author) {
        if(author.getAuthor_name().isEmpty()) {
            throw new BlankFieldExceptionHandler("The name is not Empty");
        }
        return authorRepo.insertAuthor(author);
    }

    @Override
    public void updateAuthor(Integer author_id, Author author) {
         authorRepo.updateAuthor(author_id, author);
    }

    @Override
    public void deleteAuthor(Integer author_id) {
        authorRepo.deleteAuthor(author_id);
    }
}
