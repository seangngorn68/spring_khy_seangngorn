package com.example.khy_seangngorn_class_kps.service;

import com.example.khy_seangngorn_class_kps.model.Author;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthor();
    Author getAuthorById(Integer author_id);
    Author insertAuthor(Author author);
    void updateAuthor(Integer author_id,Author author);
    void  deleteAuthor(Integer author_id);
}
