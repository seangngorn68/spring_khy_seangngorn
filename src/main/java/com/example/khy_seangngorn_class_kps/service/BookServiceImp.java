package com.example.khy_seangngorn_class_kps.service;

import com.example.khy_seangngorn_class_kps.model.Author;
import com.example.khy_seangngorn_class_kps.model.Book;
import com.example.khy_seangngorn_class_kps.model.BookRequest;
import com.example.khy_seangngorn_class_kps.model.Category;
import com.example.khy_seangngorn_class_kps.reposithory.AuthorRepo;
import com.example.khy_seangngorn_class_kps.reposithory.BookRepo;
import com.example.khy_seangngorn_class_kps.reposithory.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private final BookRepo bookRepo;
    private final AuthorRepo authorRepo;
    private final CategoryRepo categoryRepo;
    @Autowired
    public BookServiceImp(BookRepo bookRepo, AuthorRepo authorRepo, CategoryRepo categoryRepo) {
        this.bookRepo = bookRepo;
        this.authorRepo = authorRepo;
        this.categoryRepo = categoryRepo;
    }

    @Override
    public List<Book> getAllBook() {
        System.out.println(bookRepo.getAllBook());
        return bookRepo.getAllBook();
    }

    @Override
    public Book getBookById(Integer book_id) {
        System.out.println(bookRepo.getBookById(book_id));
        return bookRepo.getBookById(book_id);
    }

    @Override
    public void insertBook(BookRequest bookRequest) {
        Integer book_id = bookRepo.insertBook(bookRequest);
        System.out.println(bookRepo.getBookById(book_id));
        for (Integer category_id : bookRequest.getCategory_id()) {
            System.out.println(bookRequest.getCategory_id());
            bookRepo.insertIntoBookCategory(book_id, category_id);
        }
    }

    @Override
    public void updateBook(Integer book_id, BookRequest bookRequest) {
         Integer bookId = bookRepo.updateBook(book_id,bookRequest);
         bookRepo.deleteBookDetail(book_id);
        for(Integer category_id:bookRequest.getCategory_id()){
            bookRepo.insertIntoBookCategory(bookId, category_id);
        }

    }

    @Override
    public void deleteBook(Integer book_id) {
        bookRepo.deleteBook(book_id);
    }

    }

