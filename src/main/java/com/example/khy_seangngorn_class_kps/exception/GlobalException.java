package com.example.khy_seangngorn_class_kps.exception;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.net.URI;
import java.time.LocalDateTime;

@RestControllerAdvice
public class GlobalException {
    @ExceptionHandler(AuthorNotFoundException.class)
    ProblemDetail handleAuthorNotFoundException(AuthorNotFoundException e){
        ProblemDetail problemDetail=ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND,e.getMessage());
        //setErrHTTP
        problemDetail.setType(URI.create("localhost:8811/error/"));
        //setTitle
        problemDetail.setTitle("Not Found!!!");
        //set Time
        problemDetail.setProperty("time", LocalDateTime.now());

        return problemDetail;
    }
    //handle blank exception
    @ExceptionHandler(BlankFieldExceptionHandler.class)
    ProblemDetail  BlankFieldExceptionHandler(BlankFieldExceptionHandler e){
       return  ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST,e.getMessage());
    }

}
