package com.example.khy_seangngorn_class_kps.exception;

public class BlankFieldExceptionHandler extends RuntimeException {
    public BlankFieldExceptionHandler(String message){
        super(message);
    }

}
