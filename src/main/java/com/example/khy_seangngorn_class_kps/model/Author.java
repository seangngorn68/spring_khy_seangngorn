package com.example.khy_seangngorn_class_kps.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Author {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer author_id;
    private String author_name;
    private String gender;

}
