package com.example.khy_seangngorn_class_kps.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Book {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer book_id;
    private String title ;
    private LocalDateTime publish_date;
    private Author author;
    private List<Category> category;
}
