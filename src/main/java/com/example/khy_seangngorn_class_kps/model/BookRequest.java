package com.example.khy_seangngorn_class_kps.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class BookRequest {
    private String title ;
    private LocalDateTime publish_date;
    private Integer author_id;
    private List<Integer> category_id;
}
