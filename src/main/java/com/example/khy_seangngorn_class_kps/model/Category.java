package com.example.khy_seangngorn_class_kps.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Category {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer category_id;
    private String category_name;
}
