package com.example.khy_seangngorn_class_kps.reposithory;

import com.example.khy_seangngorn_class_kps.model.Book;
import com.example.khy_seangngorn_class_kps.model.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepo {

    @Select("""
              Select * From  book;
            """)
    @Result(property = "book_id",column = "book_id")
    @Result(property = "author",column="author_id"
            ,
    one= @One(select = "com.example.khy_seangngorn_class_kps.reposithory.AuthorRepo.getAuthorById")
    )
    @Result(property = "publish_date",column = "published_date")
    @Result(property = "category",column = "book_id",

    many =@Many(select = "com.example.khy_seangngorn_class_kps.reposithory.CategoryRepo.getAllBook")
    )
    List<Book> getAllBook();
    @Select("""
            Select * from book where book_id= #{book_id}
            """)
    @Result(property = "book_id",column = "book_id")
    @Result(property = "author",column="author_id"
            ,
            one= @One(select = "com.example.khy_seangngorn_class_kps.reposithory.AuthorRepo.getAuthorById")
    )
    @Result(property = "publish_date",column = "published_date")
    @Result(property = "category",column = "book_id",
            many =@Many(select = "com.example.khy_seangngorn_class_kps.reposithory.CategoryRepo.getAllBook")

    )
    Book getBookById(Integer book_id);

    @Select("""
            INSERT INTO book(title,published_date,author_id)
            VALUES (#{book.title},#{book.publish_date},#{book.author_id})
            RETURNING book_id
            """)
//    @Result(property = "publish_date", column = "published_date")
    Integer insertBook (@Param("book") BookRequest bookRequest);

    @Insert("""
            INSERT INTO book_detail (book_id,category_id)
            VALUES (#{book_id},#{category_id})
            """)
   void insertIntoBookCategory(Integer book_id, Integer category_id);
    @Select("""
            update book
           set title=#{book.title},published_date=#{book.publish_date},author_id=#{book.author_id} where book_id =#{book_id} 
            RETURNING book_id
            """)
//    @Result(property = "publish_date",column = "published_date")
    Integer updateBook (Integer book_id ,@Param("book") BookRequest bookRequest);


    @Select("""
            delete from book_detail where  book_id=#{book_id}
            """)
    void deleteBookDetail(Integer book_id);
    @Select("""
            delete from book where  book_id=#{book_id}
            """)
    void deleteBook(Integer bookId);
}
