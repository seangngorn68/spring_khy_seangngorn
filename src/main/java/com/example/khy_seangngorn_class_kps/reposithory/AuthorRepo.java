package com.example.khy_seangngorn_class_kps.reposithory;

import com.example.khy_seangngorn_class_kps.model.Author;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorRepo {


    @Select("""
            Select * From  author
            """)
   List<Author> getAllAuthor();
    @Select("""
            Select * from author where author_id=#{author_id}
            """)
    Author getAuthorById(Integer authorId);
    @Select("""
            INSERT INTO author VALUES (default, #{author.author_name},#{author.gender})
                        RETURNING *
            """)
    Author insertAuthor(@Param("author") Author author);
    @Update("""
            Update author set author_name=#{author.author_name},gender=#{author.gender} where author_id = #{author_id}
            """)
    void updateAuthor(Integer author_id, @Param("author") Author author);
    @Delete("""
            Delete from author where author_id=#{author_id}
            """)
    void deleteAuthor(Integer authorId);
}
