package com.example.khy_seangngorn_class_kps.reposithory;

import com.example.khy_seangngorn_class_kps.model.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Mapper
public interface CategoryRepo {
    @Select("""
              select c.category_id,c.category_name from category c inner join book_detail bc on c.category_id=bc.category_id where book_id=#{book_id};
            """)
    List<Category> getAllBook(@Param("book_id") Integer book_id);
    @Select("""
            Select * from category where category_id=#{category_id}
            """)
    Category getCategoryById(Integer categoryId);
    @Select("""
            INSERT INTO category VALUES (default, #{category.category_name})
                        RETURNING *
            """)
    Category insertCategory(@Param("category") Category category);
    @Update("""
            Update category set category_name=#{category.category_name} where category_id = #{category_id}
            """)
    void updateCategory(Integer category_id,@Param("category") Category category);
    @Delete("""
            Delete from category where category_id=#{category_id}
            """)
    void deleteCategory(Integer categoryId);
    @Select("""
              Select * From  Category;
            """)
    List<Category> getAllCategory();
}
